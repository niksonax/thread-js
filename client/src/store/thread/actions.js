import { createAction } from '@reduxjs/toolkit';
import {
  comment as commentService,
  post as postService
} from 'src/services/services';

const ActionType = {
  ADD_POST: 'thread/add-post',
  LOAD_MORE_POSTS: 'thread/load-more-posts',
  SET_ALL_POSTS: 'thread/set-all-posts',
  SET_EXPANDED_POST: 'thread/set-expanded-post',
  UPDATE_POST: 'thread/update-post'
};

const setPosts = createAction(ActionType.SET_ALL_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addMorePosts = createAction(ActionType.LOAD_MORE_POSTS, posts => ({
  payload: {
    posts
  }
}));

const addPost = createAction(ActionType.ADD_POST, post => ({
  payload: {
    post
  }
}));

const setExpandedPost = createAction(ActionType.SET_EXPANDED_POST, post => ({
  payload: {
    post
  }
}));

const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPosts(posts));
};

const loadMorePosts = filter => async (dispatch, getRootState) => {
  const {
    posts: { posts }
  } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts.filter(
    post => !(posts && posts.some(loadedPost => post.id === loadedPost.id))
  );
  dispatch(addMorePosts(filteredPosts));
};

const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPost(post));
};

const createPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPost(newPost));
};

const updatePost = (postId, postPayload) => async (dispatch, getRootState) => {
  const updatedPost = await postService.updatePost(postId, { body: postPayload });

  const mapUpdate = post => ({
    ...post,
    body: updatedPost.body
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapUpdate(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapUpdate(expandedPost)));
  }
};

const deletePost = postId => async (dispatch, getRootState) => {
  postService.deletePost(postId);

  const {
    posts: { posts }
  } = getRootState();
  const updated = posts.filter(post => post.id !== postId);

  dispatch(setPosts(updated));
};

const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPost(post));
};

const likePost = postId => async (dispatch, getRootState) => {
  const { id, isDislike } = await postService.likePost(postId);

  let diff = -1;
  if (isDislike) { // if this post was disliked - user can`t put like
    diff = 0;
  } else if (id) {
    diff = 1;
  }

  const mapLikes = post => ({
    ...post,
    likeCount: String(Number(post.likeCount) + diff) // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapLikes(expandedPost)));
  }
};

const dislikePost = postId => async (dispatch, getRootState) => {
  const { id, isLike } = await postService.dislikePost(postId);

  let diff = -1;
  if (isLike) { // if this post was liked - user can`t put dislike
    diff = 0;
  } else if (id) {
    diff = 1;
  }

  const mapDislikes = post => ({
    ...post,
    dislikeCount: String(Number(post.dislikeCount) + diff) // diff is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPost(mapDislikes(expandedPost)));
  }
};

const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const updateComment = (commentId, commentPayload) => async (dispatch, getRootState) => {
  const updatedComment = await commentService.updateComment(commentId, { body: commentPayload });

  const mapComments = post => {
    const updatedComments = [...(post.comments || [])].map(comment => (
      commentId === comment.id ? { ...comment, body: updatedComment.body } : comment
    ));
    return ({
      ...post,
      comments: updatedComments
    });
  };

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== updatedComment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === updatedComment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

const deleteComment = commentId => async (dispatch, getRootState) => {
  const comment = await commentService.getComment(commentId);
  commentService.deleteComment(commentId);

  const mapComments = post => {
    const updatedComments = [...(post.comments || [])].filter(comm => commentId !== comm.id);
    return ({
      ...post,
      commentCount: Number(post.commentCount) - 1,
      comments: updatedComments
    });
  };

  const {
    posts: { posts, expandedPost }
  } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId ? post : mapComments(post)));

  dispatch(setPosts(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPost(mapComments(expandedPost)));
  }
};

export {
  setPosts,
  addMorePosts,
  addPost,
  setExpandedPost,
  loadPosts,
  loadMorePosts,
  applyPost,
  createPost,
  updatePost,
  deletePost,
  toggleExpandedPost,
  likePost,
  dislikePost,
  addComment,
  updateComment,
  deleteComment
};
