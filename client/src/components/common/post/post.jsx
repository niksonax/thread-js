import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { IconName, ButtonColor, ButtonType } from 'src/common/enums/enums';
import { postType } from 'src/common/prop-types/prop-types';
import { Icon, Card, Image, Label, Form, Button } from 'src/components/common/common';

import styles from './styles.module.scss';

const Post = ({
  post,
  userId,
  onPostLike,
  onPostDislike,
  onPostUpdate,
  onPostDelete,
  onExpandedPostToggle,
  sharePost
}) => {
  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    createdAt
  } = post;
  const date = getFromNowTime(createdAt);

  const [isEditing, setIsEditing] = React.useState(false);
  const [editedPostBody, setEditedPostBody] = React.useState(body);

  const userOwnPost = user.id === userId;
  const handlePostEdit = () => setIsEditing(true);

  const handlePostLike = () => onPostLike(id);
  const handlePostDislike = () => onPostDislike(id);
  const handlePostDelete = () => onPostDelete(id);
  const handleExpandedPostToggle = () => onExpandedPostToggle(id);
  const handlePostUpdate = () => {
    onPostUpdate(id, editedPostBody);
    setIsEditing(false);
  };

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta style={{ display: 'flex', justifyContent: 'space-between' }}>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
          {userOwnPost ? (
            <span>
              <Label basic as="a" className={`${styles.toolbarBtn} ${styles.unselectable}`} onClick={handlePostEdit}>
                <Icon name={IconName.EDIT} />
              </Label>
              <Label basic as="a" className={`${styles.toolbarBtn} ${styles.unselectable}`} onClick={handlePostDelete}>
                <Icon name={IconName.DELETE} />
              </Label>
            </span>
          )
            : null}
        </Card.Meta>
        <Card.Description>
          {isEditing ? (
            <Form onSubmit={handlePostUpdate}>
              <Form.TextArea
                name="body"
                value={editedPostBody}
                onChange={ev => setEditedPostBody(ev.target.value)}
              />
              <div className={styles.btnWrapper}>
                <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                  Edit
                </Button>
              </div>
            </Form>
          ) : body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra className={styles.unselectable}>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostLike}
        >
          <Icon name={IconName.THUMBS_UP} />
          {likeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handlePostDislike}
        >
          <Icon name={IconName.THUMBS_DOWN} />
          {dislikeCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={handleExpandedPostToggle}
        >
          <Icon name={IconName.COMMENT} />
          {commentCount}
        </Label>
        <Label
          basic
          size="small"
          as="a"
          className={styles.toolbarBtn}
          onClick={() => sharePost(id)}
        >
          <Icon name={IconName.SHARE_ALTERNATE} />
        </Label>
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: postType.isRequired,
  userId: PropTypes.string.isRequired,
  onPostLike: PropTypes.func.isRequired,
  onPostDislike: PropTypes.func.isRequired,
  onPostUpdate: PropTypes.func.isRequired,
  onPostDelete: PropTypes.func.isRequired,
  onExpandedPostToggle: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default Post;
