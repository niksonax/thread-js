import * as React from 'react';
import PropTypes from 'prop-types';
import { getFromNowTime } from 'src/helpers/helpers';
import { DEFAULT_USER_AVATAR } from 'src/common/constants/constants';
import { Comment as CommentUI, Icon, Label, Button, Form } from 'src/components/common/common';
import { IconName, ButtonColor, ButtonType } from 'src/common/enums/enums';
import { commentType } from 'src/common/prop-types/prop-types';

import styles from './styles.module.scss';

const Comment = ({
  comment: { id, body, createdAt, user },
  userId,
  onCommentUpdate,
  onCommentDelete
}) => {
  const [isEditing, setIsEditing] = React.useState(false);
  const [editedCommentBody, setEditedCommentBody] = React.useState(body);

  const handleCommentEdit = () => setIsEditing(true);

  const handleCommentUpdate = () => {
    onCommentUpdate(id, editedCommentBody);
    setIsEditing(false);
  };

  const handleCommentDelete = () => onCommentDelete(id);

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={user.image?.link ?? DEFAULT_USER_AVATAR} />
      <CommentUI.Content>
        <CommentUI.Author as="a">{user.username}</CommentUI.Author>
        <CommentUI.Metadata>
          {getFromNowTime(createdAt)}
          {user.id === userId ? (
            <span className={`${styles.unselectable}`}>
              <Label basic as="a" className={`${styles.toolbarBtn}`} onClick={handleCommentEdit}>
                <Icon name={IconName.EDIT} style={{ padding: 0 }} />
              </Label>
              <Label basic as="a" className={`${styles.toolbarBtn}`} onClick={handleCommentDelete}>
                <Icon name={IconName.DELETE} />
              </Label>
            </span>
          ) : null}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {isEditing ? (
            <Form onSubmit={handleCommentUpdate}>
              <Form.TextArea
                name="body"
                value={editedCommentBody}
                placeholder={body}
                onChange={ev => setEditedCommentBody(ev.target.value)}
              />
              <div className={styles.btnWrapper}>
                <Button color={ButtonColor.BLUE} type={ButtonType.SUBMIT}>
                  Edit
                </Button>
              </div>
            </Form>
          ) : body}
        </CommentUI.Text>
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: commentType.isRequired,
  userId: PropTypes.string.isRequired,
  onCommentUpdate: PropTypes.func.isRequired,
  onCommentDelete: PropTypes.func.isRequired
};

export default Comment;
