import * as React from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import { threadActionCreator } from 'src/store/actions';
import { Spinner, Post, Modal, Comment as CommentUI } from 'src/components/common/common';
import AddComment from '../add-comment/add-comment';
import Comment from '../comment/comment';
import { getSortedComments } from './helpers/helpers';

const ExpandedPost = ({
  userId,
  sharePost
}) => {
  const dispatch = useDispatch();
  const { post } = useSelector(state => ({
    post: state.posts.expandedPost
  }));

  const handlePostUpdate = React.useCallback((id, postPayload) => (
    dispatch(threadActionCreator.updatePost(id, postPayload))
  ), [dispatch]);

  const handlePostLike = React.useCallback(id => (
    dispatch(threadActionCreator.likePost(id))
  ), [dispatch]);

  const handlePostDislike = React.useCallback(id => (
    dispatch(threadActionCreator.dislikePost(id))
  ), [dispatch]);

  const handleCommentAdd = React.useCallback(commentPayload => (
    dispatch(threadActionCreator.addComment(commentPayload))
  ), [dispatch]);

  const handleExpandedPostToggle = React.useCallback(id => (
    dispatch(threadActionCreator.toggleExpandedPost(id))
  ), [dispatch]);

  const handlePostDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deletePost(id), handleExpandedPostToggle())
  ), [dispatch, handleExpandedPostToggle]);

  const handleExpandedPostClose = () => handleExpandedPostToggle();

  const handleCommentUpdate = React.useCallback((id, commentPayload) => (
    dispatch(threadActionCreator.updateComment(id, commentPayload))
  ), [dispatch]);

  const handleCommentDelete = React.useCallback(id => (
    dispatch(threadActionCreator.deleteComment(id))
  ), [dispatch]);

  const sortedComments = getSortedComments(post.comments ?? []);

  return (
    <Modal
      dimmer="blurring"
      centered={false}
      open
      onClose={handleExpandedPostClose}
    >
      {post ? (
        <Modal.Content>
          <Post
            post={post}
            userId={userId}
            onPostLike={handlePostLike}
            onPostDislike={handlePostDislike}
            onPostUpdate={handlePostUpdate}
            onPostDelete={handlePostDelete}
            onExpandedPostToggle={handleExpandedPostToggle}
            sharePost={sharePost}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <h3>Comments</h3>
            {sortedComments.map(comment => (
              <Comment
                key={comment.id}
                comment={comment}
                userId={userId}
                onCommentUpdate={handleCommentUpdate}
                onCommentDelete={handleCommentDelete}
              />
            ))}
            <AddComment postId={post.id} onCommentAdd={handleCommentAdd} />
          </CommentUI.Group>
        </Modal.Content>
      ) : (
        <Spinner />
      )}
    </Modal>
  );
};

ExpandedPost.propTypes = {
  userId: PropTypes.string.isRequired,
  sharePost: PropTypes.func.isRequired
};

export default ExpandedPost;
