/* eslint-disable no-else-return */
class Post {
  constructor({ postRepository, postReactionRepository }) {
    this._postRepository = postRepository;
    this._postReactionRepository = postReactionRepository;
  }

  getPosts(filter) {
    return this._postRepository.getPosts(filter);
  }

  getPostById(id) {
    return this._postRepository.getPostById(id);
  }

  create(userId, post) {
    return this._postRepository.create({
      ...post,
      userId
    });
  }

  updatePost(postId, postBody) {
    return this._postRepository.updateById(postId, postBody);
  }

  async deletePost(postId) {
    return this._postRepository.deleteById(postId);
  }

  async setReaction(userId, { postId, isLike, isDislike }) {
    const updateOrDelete = react => {
      if (react.isLike === isLike || react.isDislike === isDislike) {
        return this._postReactionRepository.deleteById(react.id);
      } else if ((react.isLike && isDislike) || (react.isDislike && isLike)) {
        return {};
      } else {
        return this._postReactionRepository.updateById(react.id, { isLike, isDislike });
      }
    };

    const reaction = await this._postReactionRepository.getPostReaction(
      userId,
      postId
    );

    const result = reaction
      ? await updateOrDelete(reaction)
      : await this._postReactionRepository.create({ userId, postId, isLike, isDislike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
      ? {}
      : this._postReactionRepository.getPostReaction(userId, postId);
  }
}

export { Post };
