import { CommentsApiPath } from '../../common/enums/enums';

const initComment = (Router, services) => {
  const { comment: commentService } = services;
  const router = Router();

  router
    .get(CommentsApiPath.$ID, (req, res, next) => commentService
      .getCommentById(req.params.id)
      .then(comment => res.send(comment))
      .catch(next))
    .post(CommentsApiPath.ROOT, (req, res, next) => commentService
      .create(req.user.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .put(CommentsApiPath.$ID, (req, res, next) => commentService
      .updateComment(req.params.id, req.body)
      .then(comment => res.send(comment))
      .catch(next))
    .delete(CommentsApiPath.$ID, req => commentService
      .deleteComment(req.params.id));

  return router;
};

export { initComment };
